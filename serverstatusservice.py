﻿import sys, traceback
import time
import os

from datetime import datetime, timedelta
from random import randint

from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
from twisted.internet.task import LoopingCall
from twisted.python import log

import q3util
import json

from urllib.request import urlopen, Request
import urllib

ET_GETINFO = 'getinfo'
ET_GETSTATUS = 'getinfo' #'getstatus'
TIMEOUT = 10

# purge offline servers after 60 seconds
OFFLINE_SERVER_PURGE = 2

DEBUG_CHALLENGES = False
VERBOSE = False
DEBUG = True

SERVER_API = 'servers-simple/'


class ServerinfoReciever(DatagramProtocol):
    resends = {}

    def __init__(self, auth_server_api, listen_port):
        self.resends = {}  # resend last package if no response
        self.packet_prefix = '\xff' * 4
        self.serverdict = {}
        self.num_servers = -1
        self.last_index = 0
        self.auth_server_api = auth_server_api
        self.listen_port = listen_port

    def startProtocol(self):
        self.looping_call = LoopingCall(self.get_servers)
        self.looping_call.start(2, now=False)

        self.inforequest_call = LoopingCall(self.send_info_requests)
        self.inforequest_call.start(0.5)

        self.syncdata_call = LoopingCall(self.send_sync_database)
        self.syncdata_call.start(1, now=False)

    def sendMessage(self, message, address, calls=0):
        """
        Sends a message witht the correct prefix
        """

        # first management of resends of udp-packets
        if address in self.resends:
            task = self.resends[address]
            del self.resends[address]
            try:
                task.cancel()  # send more often than the timeout?
            except:
                pass
        calls += 1
        if calls <= 3:
            self.resends[address] = (reactor.callLater(10, self.sendMessage,
                    message, address, calls=calls))

        # finally send the message
        #data = '%s%s\n' % (self.packet_prefix, message)
        data = message.encode()
        prefix = self.packet_prefix.encode()

        encoded_data = b'%c%c%c%c%s' % (0xff,0xff,0xff,0xff, data)
        self.transport.write(encoded_data, address)

    def send_message(self, host, port, msg): 
        if VERBOSE:
            log.msg('[%s] >> OUT to %s:%s, msg: %s' % (self.listen_port, host, port, msg))
        self.sendMessage(msg, (host, int(port)))
        
    def send_query(self, serveraddr, msg):
        challenge = q3util.build_challenge()
        host, port = serveraddr.split(':')

        # remember the challenge so servers can't be spoofed by malicious servers. Also save the timestamp for diagnostic purposes
        self.serverdict[serveraddr]['challenges'][challenge] = datetime.now()
        
        # send the message
        self.send_message(host, port, msg + " " + challenge)

    def send_infoquery(self, args):
        serveraddr = args['serveraddr']
        self.send_query(serveraddr, ET_GETINFO)

    def send_statusquery(self, serveraddr):
        self.send_query(serveraddr, ET_GETSTATUS)

    def delete_server(self, addr):
        print('delete server')
        server_object = self.serverdict[addr]

        url = '%s%d/' % (self.auth_server_api + SERVER_API, server_object['id'])
        request = urllib.request.Request(url=url, method='DELETE')
        response = urlopen(request)
        if response.msg != 'No Content':
            log.msg('Failed to delete server')

        log.msg('Deleted server')

    def check_purge(self, addr):
        server_object = self.serverdict[addr]
        if 'purge_timeout' not in server_object:
            print('purge timeout not found, adding it')
            server_object['purge_timeout'] = datetime.now() + timedelta(seconds=OFFLINE_SERVER_PURGE)

        print('purgetimeout=%s'%server_object['purge_timeout'])

        if datetime.now() < server_object['purge_timeout']:
            return

        if VERBOSE:
            log.msg('Purging server with address: %s'%addr)

        # remove the object from the database
        self.delete_server(addr)

        # remove the server_object from the dict
        del self.serverdict[addr]


    def set_server_status(self, addr, status):
        server_object = self.serverdict[addr]
        url = '%s%d/'%(self.auth_server_api + SERVER_API, server_object['id'])
        
        # the body for the request
        json_object = json.dumps({'status': status})
        data = json_object.encode()

        headers = {'Content-Type':'application/json'}
        request = urllib.request.Request(url=url, headers=headers, method='PATCH')
        response = urlopen(request, data)
    
        if response.msg != 'OK':
            server_object['status'] = 'down'
            print("Failed to to set server status to 'down'")
            return False
        
        print("Server status is now 'down'")
        return True

    # used to check if live servers are really alive, sets status to down otherwise
    def check_timeout(self, addr):
        server_object = self.serverdict[addr]
        if 'reply_timestamp' not in server_object:
            server_object['reply_timestamp'] = datetime.now()

        start = server_object['reply_timestamp']
        elapsed = datetime.now() - start
        
        if elapsed < timedelta(seconds=TIMEOUT):
            return

        print('update server status to down')
        self.set_server_status(addr, 'down')

    def get_servers(self):
        url = self.auth_server_api + SERVER_API
        content = urlopen(url).read().decode("utf-8")
        servers = json.loads(content)
        num_servers = len(servers)

        if num_servers  != self.num_servers:
            self.num_servers = num_servers 
            log.msg('Server Count: %s' % num_servers )
            
        for server in servers:
            addr = server['host_address']

            if addr not in self.serverdict:
                self.serverdict[addr] = {'id': server['id'], 'challenges': {}}

            self.serverdict[addr]['status'] = server['status']
            
        for addr in self.serverdict.keys():
            server_object = self.serverdict[addr]
            if server_object['status'] == 'down':
                self.check_purge(addr)
            else:
                # server is online, remove the purge timeout
                if 'purge_timeout' in self.serverdict[addr]:
                    del self.serverdict[addr]['purge_timeout']

                # do check if the server has replied serverinfoResponses
                self.check_timeout(addr)
                
    def send_info_requests(self):
        max_per_second = 100
        keys = list(self.serverdict)
        key_count = len(keys)

        if(key_count == 0):
            return

        next_index = min((self.last_index + max_per_second), key_count)
        
        for index in range(self.last_index, next_index):
            self.send_statusquery(keys[index])

        if next_index == self.last_index:
            self.last_index = 0
        else:
            self.last_index = next_index
  
    def stopProtocol(self):
        "Called after all transport is teared down"
        self.looping_call.stop()
        self.inforequest_call.stop()
        self.syncdata_call.stop()

        del self.serverdict

    def parse_packet(self, data):
        cmd, payload = q3util.find_command(data);
        serverdata = {}
        if cmd == 'infoResponse':
            serverdata = q3util.infostring_to_dict(payload)
        elif cmd == 'statusResponse':
            serverdata = q3util.statusresponse_to_dict(payload)
        return serverdata

    def save_serverdata(self, addr, serverdata):
        print('save_serverdata')
        pass
        #try:
        #    servermodel = Gameserver.objects.get(host_address=addr)
        #except ObjectDoesNotExist:
        #    log.err('could not find the gameserver model for addr: %s'%addr)
        #    return

        #servermodel.host_name = serverdata['hostname']
        #servermodel.num_players = serverdata['clients']
        #servermodel.max_clients = serverdata['sv_maxclients']
        #servermodel.gametype = serverdata['gametype']
        #servermodel.map_name = serverdata['mapname']
        #servermodel.game = serverdata['game']
        #servermodel.version = serverdata['version']
        #servermodel.needspass = int(serverdata['needpass'])

        #try:
        #    servermodel.save()
        #except Exception as e:
        #    log.err('Could not update server, exception: %s'%e)
    
    def is_valid_challenge(self, addr, challenge):
        serverObject = self.serverdict[addr]
        if challenge in self.serverdict[addr]['challenges']:
            delta = datetime.now() - self.serverdict[addr]['challenges'][challenge]
            if DEBUG_CHALLENGES:
                log.msg('Challenge approved, took: %d ms'%(delta.microseconds / 1000))
            del self.serverdict[addr]['challenges'][challenge]
            return True
        return False

    def process_packet(self, packet):
        addr = packet['address']
        packetdata = packet['data']
        serverdata = self.parse_packet(packetdata)

        if len(serverdata) == 0:
            log.msg('could not parse packet with data \'%r\''%packetdata)
            return
        
        if not self.is_valid_challenge(addr, serverdata['challenge']):
            log.msg('[%s] server sent an invalid challenge' % addr)
            return

        server_object = self.serverdict[addr]
        server_object['reply_timestamp'] = datetime.now() 
        server_object['data'] = serverdata
           

    def send_sync_database(self):
        for addr in self.serverdict.keys():
            server_object = self.serverdict[addr]
            
            if 'data' not in server_object or len(server_object['data']) == 0:
                continue

            if server_object['reply_timestamp'] < datetime.now() - timedelta(seconds=5):
                continue

            if server_object['status'] != 'up':
                continue

            self.save_serverdata(addr, server_object['data'])
        

    def has_prefix(self, data):
        if len(data) < 4: 
            return False

        if data[0] == 0xff and data[1] == 0xff and data[2] == 0xff and data[3] == 0xff:
            return True

        return False

    def datagramReceived(self, data, full_address):
        (host, port) = full_address
        addr = '%s:%d'%(host, port)
        if VERBOSE:
            log.msg('[%s] << IN from %s' % (self.listen_port, addr))

        if addr not in self.serverdict:
            if VERBOSE:
                log.msg('address "%s" not found in severDict' % addr)
            return
        
        if self.has_prefix(data):
            decoded_data = data[4:].decode('utf-8')
            packet = {'data':decoded_data, 'address': addr}
            self.process_packet(packet)
        elif VERBOSE: 
            log.msg('packet did not start with prefix, data=%r' % decoded_data)
