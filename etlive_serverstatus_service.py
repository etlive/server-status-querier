﻿import os
import sys

from twisted.application import internet, service, app
from twisted.web import server, resource, static
from twisted.python import threadpool, log
from twisted.internet import reactor
            
from serverstatusservice import ServerinfoReciever

AUTH_SERVER_API = 'http://dockerhost:8000/' 
ETL_MASTER_LISTEN_PORT = int(os.getenv('ETL_MASTER_LISTEN_PORT', 27950))

# Setup Twisted application
application = service.Application('serverstatus_service')

service.IService(application).startService()
app.startApplication(application, False)

reactor.addSystemEventTrigger('before', 'shutdown', service.IService(application).stopService)

# Initialize logging
log.startLogging(sys.stdout)

serverinfoReciever = ServerinfoReciever(AUTH_SERVER_API, ETL_MASTER_LISTEN_PORT)
reactor.listenMulticast(ETL_MASTER_LISTEN_PORT, serverinfoReciever, listenMultiple=True)


reactor.run()